package pe.claro.services.sales.loginauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import pe.com.claro.maverick.runner.StarterWebApplication;



/**
 * Main class for running Spring Boot framework.<br/>
 * <b>Class</b>: LoginAuthentificationApplication<br/>
 * <b>Copyright</b>: &copy; 2020 Claro.<br/>
 * <b>Company</b>: Claro.<br/>
 *
 * @author Claro (Cla) <br/>
 * <u>Service Provider</u>: Everis Peru SAC <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li>Juan Carlos Hilario</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>Set 23, 2018 Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@SpringBootApplication
public class LoginAuthenticationApplication extends StarterWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginAuthenticationApplication.class, args);
	}

}
